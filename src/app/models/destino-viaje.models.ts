
export class DestinoViaje {
    selected: boolean;
    id:number;
    servicios: string[];
    constructor(public nombre:string , imagenurl:string , public votes: number = 0) { 
        this.servicios = ['pileta','desayuno'];
    }
    
    setSelected(s: boolean) {
        this.selected = s;
    }
    isSelected() {
      return this.selected;
    }

    voteUp(): any {
        this.votes++;
      }
      voteDown(): any {
        this.votes--;
      }
    
}


