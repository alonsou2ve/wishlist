import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {StoreModule as NgRxStoreModule, ActionReducerMap} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './lista-destinos/lista-destinos.component';
import { DestinoDetalleComponent } from './destino-detalle/destino-detalle.component';
import { DestinosApiClient } from './models/destinos-api-client.models';
import { from } from 'rxjs';
import { Route } from '@angular/compiler/src/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { FormDestinoViajeComponent } from './form-destino-viaje/form-destino-viaje.component';
import {DestinosViajesState, reducerDestinosViajes, intializeDestinosViajesState, DestinosViajesEffects} from './models/destinos-viajes-state.model';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';


const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component: ListaDestinosComponent},
  {path: 'destino', component: DestinoDetalleComponent}
];

export interface AppState {
  destinos: DestinosViajesState;
}

const reducer: ActionReducerMap <AppState> = {
  destinos: reducerDestinosViajes
};

let reducerInitialState = {
   destinos: intializeDestinosViajesState()
}

@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListaDestinosComponent,
    DestinoDetalleComponent,
    FormDestinoViajeComponent,
  ],
  imports: [
    BrowserModule,
    NgRxStoreModule.forRoot(reducer, {initialState: reducerInitialState}),
    EffectsModule.forRoot([DestinosViajesEffects]),
    RouterModule.forRoot(
      routes,
      // <--  { enableTracing: true } debugging purposes only
    ),
    FormsModule,
    ReactiveFormsModule,
    StoreDevtoolsModule.instrument({maxAge: 25})
  ],
  
  providers: [DestinosApiClient],
  bootstrap: [AppComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],

})
export class AppModule { }
